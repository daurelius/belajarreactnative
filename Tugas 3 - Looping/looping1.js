console.log("LOOPING PERTAMA")
var loopCount = 0
while(loopCount<20){
    loopCount += 2
    console.log(loopCount + " - I love coding")
}
console.log("LOOPING KEDUA")
while (loopCount>=2) {
    console.log(loopCount + " - I will become a mobile developer")
    loopCount -= 2
}